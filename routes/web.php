<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'VotingController@show', 'as' => 'vote.show']);
Route::get('vote/store/{id}', ['uses' => 'VotingController@store', 'as' => 'vote.store'])->where('id', '[0-9]+');

Route::get('suggestions', ['uses' => 'SuggestionsController@show', 'as' => 'suggest.show']);
Route::post('suggestions/select', ['uses' => 'SuggestionsController@select', 'as' => 'suggest.select']);
Route::post('suggestions/store', ['uses' => 'SuggestionsController@store', 'as' => 'suggest.store']);

Route::get('shopping-list', ['uses' => 'ShoppingListController@show', 'as' => 'shop.show']);

Route::get('style-guide', function(){
	return view('style-guide');
});