<?php
namespace App\Helpers;

use Config;

class SnafooApiHelper
{
	/**
	 * The errors encountered while interacting with the API.
	 * @var array
	 */
	public $errors = [];

	public function __construct()
	{
		// silence is golden :)
	}

	/**
	 * Gets the snacks from the web service.
	 * @return Collection $snacks A collection containing the snack data.
	 */
	public function getSnacks()
	{
		// connect to the API to get the snacks
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, Config::get('site.api_base_url').'/snacks/');
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: ApiKey '.Config::get('site.api_key')]);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response           = curl_exec($ch);
		$response_code      = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$curl_error_number  = curl_errno($ch);
		$curl_error_message = curl_error($ch);

		// was there a cURL error?
		if($curl_error_number > 0)
		{
			$this->errors[] = 'A server error was encountered while requesting the snacks. Please reload this page. If you continue to receive this message, please come back at a later time.';
			error_log('SnafooApiHelper::getSnacks() -- cURL Error: ['.$curl_error_number.'] '.$curl_error_message);
			return collect([]);
		}

		// were any other errors encountered?
		if($response_code != 200)
		{
			$this->errors[] = 'A server error was encountered while requesting the snacks. Please reload this page. If you continue to receive this message, please come back at a later time.';

			// determine the error to log based on the response code
			switch($response_code)
			{
				case 400:   $message = 'Bad Request';           break;
				case 401:   $message = 'Unauthorized Access';   break;
				default:    $message = 'Unknown Error';         break;
			}

			error_log('SnafooApiHelper::getSnacks() -- Error: ['.$response_code.'] '.$message."\n\nResponse: ".print_r($response, true));
			return collect([]);
		}

		// get the snack data and verify that it is the expected result
		$snacks = json_decode($response);
		if(!is_array($snacks))
		{
			error_log('SnafooApiHelper::getSnacks() -- API response is not of expected result.'."\n\n".'Response: '.print_r($response, true));
			return collect([]);
		}

		// convert the array to a collection and verify it is as expected
		$snacksCollection   = collect($snacks);
		$firstSnack         = $snacksCollection->first();
		if(!property_exists($firstSnack, 'id') || !property_exists($firstSnack, 'name') || !property_exists($firstSnack, 'optional'))
		{
			error_log('SnafooApiHelper::getSnacks() -- API response is not of expected result.'."\n\n".'Response: '.print_r($response, true));
			return collect([]);
		}

		return $snacksCollection;
	}
}