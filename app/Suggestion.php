<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    public $timestamps  = false;
    protected $fillable = ['snack_id', 'submitted_at'];
}
