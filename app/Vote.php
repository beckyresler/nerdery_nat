<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public $timestamps  = false;
    protected $fillable = ['snack_id', 'submitted_at'];
}
