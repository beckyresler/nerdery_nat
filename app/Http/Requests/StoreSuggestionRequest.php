<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSuggestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array An array containing the validation rules.
     */
    public function rules()
    {
    	// sanitize the form request
	    $this->sanitize();

	    // set up and return the validation rules
        return [
            'snack_name'        => 'required|max:200',
	        'purchase_location' => 'required|max:50'
        ];
    }

	/**
	 * Gets the messages that correspond to the validation rules that apply to the form request.
	 * @return array An array containing the messages.
	 */
    public function messages()
    {
    	return [
    		'snack_name.required'           => 'You must enter a Name for your snack.',
		    'snack_name.max'                => 'The Name of your snack cannot exceed :max characters.',
		    'purchase_location.required'    => 'You must enter the Purchase Location for your snack.',
		    'purchase_location.max'         => 'The name of the Purchase Location for your snack cannot exceed :max characters.'
	    ];
    }

	/**
	 * Sanitizes the form request data.
	 */
    public function sanitize()
    {
    	// get all the form request data
	    $input = $this->all();

	    // set up the sanitization groups
    	$textFields = ['snack_name', 'purchase_location'];

    	// sanitize the text fields
	    foreach($textFields as $fieldName)
	    {
	    	// remove HTML tags, encode ampersands and quotes, strip high and low special characters
	    	$input[$fieldName] = filter_var(trim($input[$fieldName]), FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_AMP | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
	    }

	    // update the request with the sanitized data
	    $this->replace($input);
    }
}
