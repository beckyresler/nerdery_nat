<?php
namespace App\Http\Controllers;

use App\Helpers\SnafooApiHelper;
use App\Suggestion;
use App\Vote;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;

class VotingController extends Controller
{
	/**
	 * Shows the main voting interface.
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function show()
    {
		// initialize some things
	    $apiErrors              = [];
	    $snacksAlwaysPurchased  = collect([]);
	    $suggestedSnacks        = collect([]);
	    $userVotes              = collect([]);
	    $snackVotes             = collect([]);

	    // get the snacks
	    $apiHelper = new SnafooApiHelper();
        $allSnacks = $apiHelper->getSnacks();
        if(!empty($apiHelper->errors))
	        $apiErrors = $apiHelper->errors;

	    // make sure no errors were encountered while getting the snacks
	    if(empty($apiErrors))
	    {
	    	// get the snacks that are always purchased
			$snacksAlwaysPurchased = $allSnacks->where('optional', false)->sortBy('name');

			// get the snacks that have been suggested for this month
		    $now                = Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
		    $suggestions        = Suggestion::select('snack_id')->whereMonth('submitted_at', $now->month)->whereYear('submitted_at', $now->year)->groupBy('snack_id')->get();
		    $suggestedSnacks    = $allSnacks->whereIn('id', $suggestions->pluck('snack_id'))->sortBy('name');

		    // get the votes for the month
		    $snackVotes = Vote::select('snack_id', DB::raw('COUNT(snack_id) AS voteCount'))->whereMonth('submitted_at', $now->month)->whereYear('submitted_at', $now->year)->groupBy('snack_id')->get();

		    // get the current user's votes
		    $userVotes = $this->_getVotes();
	    }

	    return view('voting', compact('snacksAlwaysPurchased', 'suggestedSnacks', 'userVotes', 'snackVotes', 'apiErrors'));
    }

	/**
	 * Records a vote for a snack.
	 * @param id $snack_id The ID of the snack.
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function store($snack_id)
    {
	    // verify that the user has available votes remaining
	    $votes = $this->_getVotes();
	    if($votes->count() >= 3)
		    return redirect()->route('vote.show')->with('error', 'Your vote could not be recorded. You have already used all of your available votes.');

	    // verify that the snack selected was suggested for the current month
	    $now            = Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
	    $suggestions    = Suggestion::where('snack_id', $snack_id)->whereMonth('submitted_at', $now->month)->whereYear('submitted_at', $now->year)->get();
	    if($suggestions->count() == 0)
	    	return redirect()->route('vote.show')->with('error', 'You attempted to vote for a snack that has not been suggested for this month. You must suggest the snack before you can vote for it.');

	    // record the vote
	    $newVote = Vote::create(['snack_id' => $snack_id, 'submitted_at' => $now]);
	    if(empty($newVote))
	        return redirect()->route('vote.show')->with('error', 'An error was encountered while recording your vote. Please try again.');

	    // add the new vote to the collection
	    $votes->push(['snack_id' => $newVote->snack_id, 'submitted_at' => $newVote->submitted_at->timestamp]);

	    // set the cookie for the votes
	    // - set them to expire at the end of the current month
	    $expirationDate = Carbon::create($now->year, $now->month, $now->daysInMonth, 23, 59, 59, Config::get('app.timezone'));
	    setcookie('snafoo-votes', json_encode($votes), $expirationDate->timestamp, '/');

	    return redirect()->route('vote.show')->with('success', 'Your vote has been submitted.');
    }

	/**
	 * Gets the votes the user has submitted this month.
	 * @return \Illuminate\Support\Collection $validVotes A collection containing the votes.
	 */
    private function _getVotes()
    {
    	// get the current user's votes (stored in a cookie as a JSON array) and convert the array to a collection
	    $cookieVotes    = (array_key_exists('snafoo-votes', $_COOKIE)) ? json_decode($_COOKIE['snafoo-votes']) : [];
	    $votes          = (is_array($cookieVotes)) ? collect($cookieVotes) : collect([]);

    	// verify that the votes were placed this month (in case the cookie didn't expire properly)
    	$now        = Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
	    $validVotes = $votes->reject(function($value, $key) use ($now) {
	    	$voteSubmitted = Carbon::createFromTimeStamp($value->submitted_at, Config::get('app.timezone'));
	    	return ($voteSubmitted->month != $now->month || $voteSubmitted->year != $now->year) ? true : false;
	    });

    	return $validVotes;
    }
}
