<?php
namespace App\Http\Controllers;

use App\Helpers\SnafooApiHelper;
use App\Http\Requests\StoreSuggestionRequest;
use App\Suggestion;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;

class SuggestionsController extends Controller
{
	/**
	 * Shows the main suggestions interface.
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show()
    {
    	// initialize some things
	    $apiErrors          = [];
	    $snackSuggestions   = collect([]);

	    // get the needed info from the cookies
	    $suggestionMade = (array_key_exists('snafoo-suggestion-made', $_COOKIE)) ? $_COOKIE['snafoo-suggestion-made'] : false;
	    $suggestionDate = (array_key_exists('snafoo-suggestion-date', $_COOKIE)) ? $_COOKIE['snafoo-suggestion-date'] : null;
	    $suggestedSnack = (array_key_exists('snafoo-suggested-snack', $_COOKIE)) ? $_COOKIE['snafoo-suggested-snack'] : null;
	    $cookieInfo     = ['suggestion-made' => $suggestionMade, 'suggestion-date' => $suggestionDate, 'suggested-snack' => $suggestedSnack];

	    // has the user already suggested a snack this month?
	    // if so, don't get the snack info because it won't be displayed
	    if(!$suggestionMade)
	    {
	    	// get the available snacks
	        $apiHelper = new SnafooApiHelper();
	        $allSnacks = $apiHelper->getSnacks();
	        if(!empty($apiHelper->errors))
		        $apiErrors = $apiHelper->errors;

		    // make sure no errors were encountered while getting the snacks
		    if(empty($apiErrors))
		    {
		        // get the optional snacks that are available (from the API)
				$optionalSnacksAvailable = $allSnacks->where('optional', true)->sortBy('name');

				// get the snacks that have been suggested for this month
			    $now                = Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
			    $suggestedSnacks    = Suggestion::select('snack_id')->whereMonth('submitted_at', $now->month)->whereYear('submitted_at', $now->year)->groupBy('snack_id')->get();

			    // exclude any snacks already suggested for this month from the list of snacks to suggest
			    $suggestedSnackIds  = (!$suggestedSnacks->isEmpty()) ? $suggestedSnacks->pluck('snack_id') : [];
			    $optionalSnackIds   = $optionalSnacksAvailable->pluck('id');
			    $remainingSnackIds  = $optionalSnackIds->diff($suggestedSnackIds);
			    $snackSuggestions   = $optionalSnacksAvailable->whereIn('id', $remainingSnackIds);
		    }
	    }

	    return view('suggestions', compact('snackSuggestions', 'apiErrors', 'cookieInfo'));
    }

	/**
	 * Records a selected snack suggestion (from previously set options).
	 * @param Request $request The incoming form request.
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function select(Request $request)
    {
	    // get the form data
	    $selectedSnackId = $request->get('snack_id', null);

	    // make sure a snack was selected
	    if(empty($selectedSnackId))
	    	return redirect()->route('suggest.show')->withInput()->with('select_error', 'Please select a snack.');

	    // make sure the snack selected hasn't already been suggested for this month
	    $now            = Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
	    $suggestions    = Suggestion::where('snack_id', $selectedSnackId)->whereMonth('submitted_at', $now->month)->whereYear('submitted_at', $now->year)->get();
	    if($suggestions->count() > 0)
	        return redirect()->route('suggest.show')->withInput()->with('select_error', 'The snack that you selected has already been suggested for this month. Please suggest a different snack.');

	    // get the snacks from the API (will need the name of the selected snack to store in the cookie)
        $apiHelper = new SnafooApiHelper();
        $allSnacks = $apiHelper->getSnacks();
        if(!empty($apiHelper->errors))
        {
        	error_log('SuggestionsController::store() -- Error getting snacks from API.');
			return redirect()->route('suggest.show')->withInput()->with('select_error', 'There were issues submitting your snack suggestion. <ul><li>'.implode('</li><li>', $apiHelper->errors).'</li></ul>');
        }

        // get the name of the selected snack
	    $selectedSnack = $allSnacks->where('id', $selectedSnackId)->first();

	    // record the suggestion
	    $suggestion = Suggestion::create(['snack_id' => $selectedSnackId, 'submitted_at' => $now]);
	    if(empty($suggestion))
	    	return redirect()->route('suggest.show')->withInput()->with('select_error', 'An error was encountered while recording your snack suggestion. Please try again.');

	    // set cookies for the snack suggestion
	    // - set them to expire at the end of the current month
	    $expirationDate = Carbon::create($now->year, $now->month, $now->daysInMonth, 23, 59, 59, Config::get('app.timezone'));
	    setcookie('snafoo-suggestion-made', true, $expirationDate->timestamp, '/');
	    setcookie('snafoo-suggestion-date', $now->timestamp, $expirationDate->timestamp, '/');
	    setcookie('snafoo-suggested-snack', $selectedSnack->name, $expirationDate->timestamp, '/');

    	return redirect()->route('suggest.show')->with('success', 'Your snack suggestion has been successfully added to this month\'s suggestions.');
    }

	/**
	 * Records a new snack suggestion.
	 * @param StoreSuggestionRequest $request The incoming form request (after validation).
	 * @return \Illuminate\Http\RedirectResponse
	 */
    public function store(StoreSuggestionRequest $request)
    {
    	// get the form data (validation has already been done in the FormRequest)
	    $snackName          = $request->get('snack_name');
	    $purchaseLocation   = $request->get('purchase_location');

	    // get the snacks from the API
        $apiHelper = new SnafooApiHelper();
        $allSnacks = $apiHelper->getSnacks();
        if(!empty($apiHelper->errors))
        {
        	error_log('SuggestionsController::store() -- Error getting snacks from API.');
			return redirect()->route('suggest.show')->withInput()->with('store_error', 'There were issues submitting your snack suggestion. <ul><li>'.implode('</li><li>', $apiHelper->errors).'</li></ul>');
        }

	    // make sure the snack hasn't already been suggested (do a case insensitive comparison)
	    if($allSnacks->contains(function($value, $key) use ($snackName) { return strtolower($value->name) == strtolower($snackName); }))
		    return redirect()->route('suggest.show')->withInput()->with('store_error', 'The snack that you suggested has already been suggested by someone else. Please suggest a new snack.<br /><br /><i class="fa fa-info-circle"></i> NOTE: The following snacks have already been suggested: '.implode(', ', $allSnacks->sortBy('name')->pluck('name')->toArray()));

	    // send the snack info to the API
	    $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, Config::get('site.api_base_url').'/snacks/');
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: ApiKey '.Config::get('site.api_key'), 'Content-Type: text/json']);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['name' => $snackName, 'location' => $purchaseLocation]));
		$response           = curl_exec($ch);
		$response_code      = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$curl_error_number  = curl_errno($ch);
		$curl_error_message = curl_error($ch);

	    // was a cURL error encountered?
		if($curl_error_number > 0)
		{
			error_log('SuggestionsController::store() -- cURL Error: ['.$curl_error_number.'] '.$curl_error_message);
			return redirect()->route('suggest.show')->withInput()->with('store_error', 'A server error was encountered while submitting your snack suggestion. Please try again. If you continue to receive this message, please come back at a later time.');
		}

		// was an API error encountered?
		if($response_code != 200)
		{
			// determine the error to log based on the response code
			switch($response_code)
			{
				case 400:   $message = 'Bad Request';           break;
				case 401:   $message = 'Unauthorized Access';   break;
				case 409:   $message = 'Conflict';              break;
				default:    $message = 'Unknown Error';         break;
			}

			error_log('SuggestionsController::store() -- An API error has been encountered. ['.$response_code.'] '.$message."\n\nResponse: ".print_r($response, true));
			return redirect()->route('suggest.show')->withInput()->with('store_error', 'A server error was encountered while submitting your snack suggestion. Please try again. If you continue to receive this message, please come back at a later time.');
		}

		// decode the API response and verify that it is the expected result
		$newSnack = json_decode($response);
	    if(!is_object($newSnack) || !property_exists($newSnack, 'id') || !property_exists($newSnack, 'name'))
	    {
		    error_log('SuggestionsController::store() -- API response is not of expected result.'."\n\n".'Response: '.print_r($response, true));
			return redirect()->route('suggest.show')->withInput()->with('store_error', 'A server error was encountered while submitting your snack suggestion. Please try again. If you continue to receive this message, please come back at a later time.');
	    }

	    // record the suggestion
	    $now        = Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
	    $suggestion = Suggestion::create(['snack_id' => $newSnack->id, 'submitted_at' => $now]);
	    if(empty($suggestion))
	    	return redirect()->route('suggest.show')->withInput()->with('store_error', 'An error was encountered while submitting your snack suggestion. Please try again.');

	    // set cookies for the snack suggestion
	    // - set them to expire at the end of the current month
	    $expirationDate = Carbon::create($now->year, $now->month, $now->daysInMonth, 23, 59, 59, Config::get('app.timezone'));
	    setcookie('snafoo-suggestion-made', true, $expirationDate->timestamp, '/');
	    setcookie('snafoo-suggestion-date', $now->timestamp, $expirationDate->timestamp, '/');
	    setcookie('snafoo-suggested-snack', $newSnack->name, $expirationDate->timestamp, '/');

	    return redirect()->route('suggest.show')->with('success', 'Your snack suggestion has been successfully added to the system and added to this month\'s suggestions.');
    }
}
