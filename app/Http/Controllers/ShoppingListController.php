<?php
namespace App\Http\Controllers;

use App\Helpers\SnafooApiHelper;
use App\Vote;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Http\Request;

class ShoppingListController extends Controller
{
	/**
	 * Shows the main shopping list interface.
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show()
    {
    	// initialize some things
	    $apiErrors          = [];
	    $snacksToPurchase   = collect([]);

	    // get the snacks
	    $apiHelper = new SnafooApiHelper();
        $allSnacks = $apiHelper->getSnacks();
        if(!empty($apiHelper->errors))
	        $apiErrors = $apiHelper->errors;

	    // make sure no errors were encountered while getting the snacks
	    if(empty($apiErrors))
	    {
	    	// get the snacks that are always purchased
			$snacksAlwaysPurchased = $allSnacks->where('optional', false)->sortBy('purchaseLocations');

			// get the additional suggested snacks with the highest number of votes to make up a list of 10
		    $now                = Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
		    $additionalSnacks   = Vote::select('snack_id', DB::raw('COUNT(snack_id) as voteCount'))
								    ->whereMonth('submitted_at', $now->month)->whereYear('submitted_at', $now->year)->groupBy('snack_id')
								    ->orderBy('voteCount', 'DESC')->limit(10 - $snacksAlwaysPurchased->count())->get();

		    // get the snack data for the suggested snacks with the most votes
		    $suggestedSnacks = $allSnacks->whereIn('id', $additionalSnacks->pluck('snack_id'));

		    // combine the always purchased snacks and suggested snacks into one list and sort by purchase location
		    $snacksToPurchase = $snacksAlwaysPurchased->merge($suggestedSnacks)->sortBy('purchaseLocations');
	    }

	    return view('shopping-list', compact('snacksToPurchase', 'additionalSnacks', 'apiErrors'));
    }
}
