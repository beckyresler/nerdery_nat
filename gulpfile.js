var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    // copy Foundation javascript files to the resources directory
	mix.copy('bower_components/foundation/js/foundation', 'resources/assets/js/foundation');

	// copy the other javascript files being used to the public assets directory
	mix.copy('bower_components/foundation/js/vendor/fastclick.js', 'public/assets/js/fastclick.js');
	mix.copy('bower_components/foundation/js/vendor/modernizr.js', 'public/assets/js/modernizr.js');
	mix.copy('bower_components/foundation/js/vendor/jquery.js', 'public/assets/js/jquery.js');

	// merge the Foundation files being used into a single file and put it in the public assets directory
	mix.scripts([
			'foundation/foundation.js',
			'foundation/foundation.alert.js',
			'foundation/foundation.dropdown.js',
			'foundation/foundation.offcanvas.js',
			'foundation/foundation.reveal.js',
			'foundation/foundation.topbar.js'
		], 'public/assets/js/foundation.js');

});
