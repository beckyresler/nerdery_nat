# SnaFoo Snack Food Ordering System

## Development Information

This project uses [Composer](https://getcomposer.org/), [Bower](http://bower.io/), [Bundler](http://bundler.io/), [Node.js](https://nodejs.org/en/), [Gulp](http://gulpjs.com/), and [Compass](http://compass-style.org/) to manage dependencies and aid in development. **You must have these systems installed in your development environment** in order to modify this application.

This project is built on the [Laravel](http://laravel.com/) php framework. It also utilizes the [Foundation](http://foundation.zurb.com/) front end framework and the [Font Awesome](http://fontawesome.io/) icon font.

### Current Versions Utilized

* Laravel: 5.4.11
* Foundation: 5.5.3
* Font Awesome: 4.7.0

## Steps to deploy

1) Set up hosting, either locally or on a web server, depending on your needs.

2) Create a database and database user.

3) Get a copy of the website files, either from the git repository or by downloading the project zip file from Dropbox.

_From the git repository ..._

`git clone git@bitbucket.org:beckyresler/nerdery_nat.git`

_From Dropbox ..._

[https://www.dropbox.com/s/8o48i1g3fz55b7q/nat-project-files.zip?dl=0](https://www.dropbox.com/s/8o48i1g3fz55b7q/nat-project-files.zip?dl=0)

4) Rename (or copy) **.env.example** to **.env** and modify the values of the variables as needed for the application information (first block of variables), database information (second block of variables), and anything else that is necessary for your development environment.

**NOTE: The following steps will be completed via command line.**

5) Navigate to the directory containing the project.

`cd /path/to/project-name`

6) If you do not have [Composer](https://getcomposer.org/) installed globally for your development environment, install it in your project.

`curl -sS https://getcomposer.org/installer | php`

7) Install the Composer packages needed by the project.

`php composer install` or just `composer install` if Composer is installed globally.

**The following steps are only required if you cloned the project from the git repository.**

6) Install the needed development dependencies. This requires you to have [Bower](http://bower.io/), [Bundler](http://bundler.io/), and [Node.js](https://nodejs.org/en/) installed in your development environment.

`bower install`

`bundle install`

`npm install`

7) Run the following command to copy the needed javascript files and stylesheets to the appropriate locations in the project. This requires that you have [Gulp](http://gulpjs.com/) installed in your development environment.

`gulp`

**NOTE:** This command can be run again with a `--production` flag to minify the javascript files and stylesheets. This should be done before moving to a production server.

8) Run the following command to create and compile the default stylesheets. This requires that you have [Compass](http://compass-style.org/) installed in your development environment.

`bundle exec compass compile`

**The following steps are required for both methods of deployment.**

9) Run the following command to generate the application key for the project.

`php artisan key:generate`

10) Run the following command to initialize the database.

`php artisan migrate`

11) Run the project in your browser.



## Steps to set up for development

1) Set up your development environment as necessary to run the project. Since this varies depending on what solution you use (i.e. MAMP, Vagrant, Homestead, etc.), it is up to you to set things up.

2) Create a database and database user in your development environment.

3) Clone the git repository for the project.

`git clone git@bitbucket.org:beckyresler/nerdery_nat.git`

4) Rename (or copy) **.env.example** to **.env** and modify the values of the variables as needed.

**NOTE: The following steps will be completed via command line.**

5) Navigate to the directory that was created when you cloned the project repository.

`cd /path/to/project-name`

6) If you do not have Composer installed globally for your development environment, install it in your project.

`curl -sS https://getcomposer.org/installer | php`

7) Install the needed development dependencies.

`php composer install`

`bower install`

`bundle install`

`npm install`

8) Run the following command to copy the needed javascript files and stylesheets to the appropriate locations in the project.

`gulp`

**NOTE:** This command can be run again with a `--production` flag to minify the javascript files and stylesheets. This should be done before moving to a production server.

9) Run the following command to create and compile the default stylesheets. This requires that you have [Compass](http://compass-style.org/) installed in your development environment.

`bundle exec compass compile`

10) Run the following command to generate the application key for the project.

`php artisan key:generate`

11) Run the following command to initialize the database.

`php artisan migrate`

12) Run the project in your browser to verify everything is functioning properly. Then modify as necessary.
