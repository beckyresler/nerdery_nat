@extends('layouts.main')

@section('content')
    <h1>Snack Voting for {{ \Carbon\Carbon::now(new \DateTimeZone(Config::get('app.timezone')))->format('F Y') }}</h1>
    <ul>
        <li>You can vote for up to 3 snack options each month.</li>
        <li><strong>Remaining Votes:</strong> {{ (3 - $userVotes->count()) }}</li>
    </ul>

    {{-- are there any session messages? --}}
    @if(session()->has('success'))
        <div class="alert-box success"><i class="fa fa-lg fa-check-circle"></i> {{ session()->get('success') }}</div>
    @endif
    @if(session()->has('error'))
        <div class="alert-box alert"><i class="fa fa-lg fa-exclamation-circle"></i> {!! session()->get('error') !!}</div>
    @endif

    {{-- were any API errors encountered? --}}
    @if(!empty($apiErrors))
        <div class="alert-box alert">
            <ul>
                <li>{!! implode('</li><li>', $apiErrors) !!}</li>
            </ul>
        </div>
    @else
        <div class="row">
            <div class="large-7 columns">
                <h2>Suggestions for This Month</h2>

                {{-- make sure there are suggested snacks --}}
                @if($suggestedSnacks->isEmpty())
                    <p>No snacks have been suggested for this month. <a href="{{ route('suggest.show') }}">Be the first to suggest a snack</a>.</p>
                @else
                    <table class="full-width snacks">
                    <thead>
                        <tr>
                            <th class="name">Snack</th>
                            <th class="count">Votes</th>
                            <th class="purchased">Last Purchased</th>
                            <th class="actions">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($suggestedSnacks as $snack)
                        <tr>
                            <td class="name">{{ $snack->name }}</td>
                            <td class="count">
                                @if($snackVotes->contains('snack_id', $snack->id)) {{ $snackVotes->where('snack_id', $snack->id)->first()->voteCount }} @else 0 @endif
                            </td>
                            <td class="purchased">
                                @if(!empty($snack->lastPurchaseDate)) {{ $snack->lastPurchaseDate }} @else &mdash; @endif
                            </td>
                            <td class="actions">
                                {{-- has the user voted for this snack already? --}}
                                @if($userVotes->contains('snack_id', $snack->id))
                                    <i class="fa fa-check-square-o fa-lg"></i>

                                {{-- does the user have any votes remaining? --}}
                                @elseif($userVotes->count() < 3)
                                    <a href="{{ route('vote.store', ['id' => $snack->id]) }}" class="button small"><i class="fa fa-check-square-o"></i> Vote</a>

                                {{-- otherwise ... --}}
                                @else
                                    &mdash;
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    </table>

                @endif
            </div>
            <div class="large-5 columns">
                {{-- make sure there are snacks that are always purchased --}}
                @if(!$snacksAlwaysPurchased->isEmpty())
                    <h2>Always Purchased</h2>
                    <ol class="required-snacks">
                    @foreach($snacksAlwaysPurchased as $snack)
                        <li>{{ $snack->name }}</li>
                    @endforeach
                    </ol>
                @endif
            </div>
        </div>
    @endif
@endsection
