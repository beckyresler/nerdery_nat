@extends('layouts.main')

@section('page-title', 'Style Guide | ')

@section('content')

    <h1>Style Guide</h1>
    <p>This is the main content area of the website pages. <strong>Bolded text</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, <a href="#" target="_blank">tempor</a> sit amet, ante. Donec eu libero sit amet quam egestas semper. <em><a href="#" target="_blank">Italicised text</a>.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>inline code</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Inline link</a> in turpis pulvinar facilisis. Ut felis.</p>

    <h2>Headings</h2>
    <p>The following shows all levels of <strong>headings</strong> that can be used.</p>
    <h1>Heading Level 1</h1>
    <h2>Heading Level 2</h2>
    <h3>Heading Level 3</h3>
    <h4>Heading Level 4</h4>
    <h5>Heading Level 5</h5>

    <h2>Blockquotes</h2>
    <blockquote>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.</p>
        <p>Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>
    </blockquote>

    <h2>(Nested) Ordered List</h2>
    <ol>
        <li>Pellentesque habitant morbi tristique</li>
        <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            <ol>
                <li>lorem ipsum dolor sit amet</li>
                <li>consectetuer adipiscing elit</li>
                <li>lorem ipsum dolor sit amet
                    <ol>
                        <li>lorem ipsum dolor sit amet</li>
                        <li>consectetuer adipiscing elit</li>
                    </ol>
                </li>
                <li>consectetuer adipiscing elit</li>
                <li>lorem ipsum dolor sit amet</li>
            </ol>
        </li>
        <li>Aliquam tincidunt mauris eu risus.</li>
    </ol>

    <h2>(Nested) Unordered List</h2>
    <ul>
        <li>Pellentesque habitant morbi tristique</li>
        <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
            <ul>
                <li>lorem ipsum dolor sit amet</li>
                <li>consectetuer adipiscing elit</li>
                <li>lorem ipsum dolor sit amet
                    <ul>
                        <li>lorem ipsum dolor sit amet</li>
                        <li>consectetuer adipiscing elit</li>
                    </ul>
                </li>
                <li>consectetuer adipiscing elit</li>
                <li>lorem ipsum dolor sit amet</li>
            </ul>
        </li>
        <li>Aliquam tincidunt mauris eu risus.</li>
    </ul>

    <h2>Definition List</h2>
    <dl>
        <dt>Term</dt>
        <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</dd>
        <dt>Lorem ipsum dolor sit amet</dt>
        <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</dd>
    </dl>

    <h2>Tables</h2>
    <p>The following is a sample <strong>table</strong>.</p>
    <table>
        <thead>
        <tr>
            <th>Heading W</th>
            <th>Heading X</th>
            <th>Heading Y</th>
            <th>Heading Z</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Lorem ipsum</td>
            <td>Duis tempor</td>
            <td>88</td>
            <td>Lorem ipsum</td>
        </tr>
        <tr>
            <td>Duis tempor</td>
            <td>Lorem ipsum</td>
            <td>56</td>
            <td>Duis tempor</td>
        </tr>
        <tr>
            <td>Lorem ipsum</td>
            <td>Duis tempor</td>
            <td>2</td>
            <td>Lorem ipsum</td>
        </tr>
        <tr>
            <td>Duis tempor</td>
            <td>Lorem ipsum</td>
            <td>27</td>
            <td>Duis tempor</td>
        </tr>
        </tbody>
    </table>

    <h2>Panels</h2>
    <div class="panel">
        <p>Cupcake ipsum dolor sit amet gingerbread tootsie roll. Cotton candy icing muffin. I love chocolate bar chocolate cake cupcake candy. Tootsie roll oat cake caramels topping wypas chocolate bar biscuit. I love I love brownie brownie pudding. Croissant wypas pastry jelly-o bear claw sugar plum I love jujubes. Souffl&eacute; caramels cake apple pie jelly beans. Chocolate bar I love liquorice. Gummies cheesecake dessert marshmallow chupa chups. Tootsie roll I love chupa chups cake.</p>
    </div>
    <div class="panel">
        <p>Bacon ipsum dolor sit amet fugiat deserunt chicken tongue aliqua mollit, laborum non. Pork chop eiusmod short loin aute jowl fugiat, chicken chuck commodo quis nulla non. Occaecat in commodo quis, reprehenderit biltong ex. Pig cillum spare ribs sunt. Bacon officia ad proident sausage. Nisi sed in eu. In shoulder id tail t-bone voluptate.</p>
        <p>Ut sint brisket, laboris beef ribs consectetur pork. Chuck id in irure. Pariatur headcheese quis, incididunt strip steak cupidatat spare ribs eu ad velit tempor nisi aliquip ham hock. Laborum exercitation veniam consectetur, qui magna venison dolor aute fugiat salami chicken adipisicing chuck eiusmod. Non cow in venison, cupidatat salami ground round do. Occaecat commodo chuck ex, t-bone excepteur adipisicing minim ut jerky beef deserunt nostrud sint salami. Aliqua tongue tempor cow, do sint strip steak venison in sunt ball tip in ut laboris adipisicing.</p>
    </div>

    <h2>Alerts (w/ FontAwesome icons)</h2>
    <div class="alert-box alert"><i class="fa fa-lg fa-exclamation-circle"></i> ERROR alert</div>
    <div class="alert-box warning"><i class="fa fa-lg fa-exclamation-triangle"></i> WARNING alert</div>
    <div class="alert-box success"><i class="fa fa-lg fa-check-circle"></i> SUCCESS alert</div>
    <div class="alert-box info"><i class="fa fa-lg fa-info-circle"></i> INFO alert</div>
    <div class="alert-box secondary">SECONDARY alert</div>

    <h2>Buttons</h2>
    <p>Here are some sample buttons (laid out in a grid) ...</p>
    <div class="row">
        <div class="large-6 columns"><p><a class="button" href="#">Button Text</a> - button</p></div>
        <div class="large-6 columns"><p><a class="radius button" href="#">Button Text</a> - radius button</p></div>
    </div>
    <div class="row">
        <div class="large-6 columns"><p><a class="small button" href="#">Button Text</a> - small button</p></div>
        <div class="large-6 columns"><p><a class="small radius button" href="#">Button Text</a> - small radius button</p></div>
    </div>
    <div class="row">
        <div class="large-6 columns"><p><a class="tiny button" href="#">Button Text</a> - tiny button</p></div>
        <div class="large-6 columns"><p><a class="tiny radius button" href="#">Button Text</a> - tiny radius button</p></div>
    </div>
    <div class="row">
        <div class="large-6 columns"><p><a class="small round button" href="#">Button Text</a> - small round button</p></div>
        <div class="large-6 columns"><p><a class="small disabled button" href="#">Button Text</a> - small disabled button</p></div>
    </div>
    <div class="row">
        <div class="large-6 columns"><p><a class="small secondary button" href="#">Button Text</a> - small secondary button</p></div>
        <div class="large-6 columns"><p><a class="small alert button" href="#">Button Text</a> - small alert button</p></div>
    </div>
    <div class="row">
        <div class="large-6 columns"><p><a class="small success button" href="#">Button Text</a> - small success button</p></div>
        <div class="large-6 columns">&nbsp;</div>
    </div>
    <div class="row">
        <div class="large-6 columns">Button Group w/ plain buttons<br /><br />
            <ul class="button-group">
                <li><a class="small button" href="#">Button 1</a></li>
                <li><a class="small button" href="#">Button 2</a></li>
                <li><a class="small button" href="#">Button 3</a></li>
            </ul>
        </div>
        <div class="large-6 columns">Button Group w/ radius buttons<br /><br />
            <ul class="button-group radius">
                <li><a class="small button" href="#">Button 1</a></li>
                <li><a class="small button" href="#">Button 2</a></li>
                <li><a class="small button" href="#">Button 3</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">Button Bar w/ round buttons<br /><br />
            <div class="button-bar">
                <ul class="button-group round">
                    <li><a class="small button" href="#">Button 1</a></li>
                    <li><a class="small button" href="#">Button 2</a></li>
                    <li><a class="small button" href="#">Button 3</a></li>
                </ul>
                <ul class="button-group round">
                    <li><a class="small button" href="#">Button 1</a></li>
                    <li><a class="small button" href="#">Button 2</a></li>
                    <li><a class="small button" href="#">Button 3</a></li>
                </ul>
            </div>
        </div>
    </div>

    <h2>Multiple Paragraphs</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed malesuada imperdiet justo non molestie. Pellentesque nisi arcu, euismod ac luctus a, sodales sit amet sem. Sed vestibulum urna in nisl auctor dignissim. Maecenas non libero ut urna faucibus feugiat. Nam vitae tortor mollis ante dictum scelerisque tempus eu sem. Vivamus ut ante eros, id porta massa. Sed ut lacus at elit tincidunt tempor. Curabitur facilisis, libero vel porttitor faucibus, sapien arcu convallis dolor, sed facilisis metus urna non tortor. Donec quis eros eu felis imperdiet elementum. Morbi bibendum imperdiet felis nec ultricies. Nullam placerat tempor nisl ac vehicula. In pellentesque felis sed purus vestibulum in commodo leo ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras pulvinar tellus quis augue luctus laoreet. Vivamus sed suscipit erat. Duis tempor dignissim commodo. Nullam bibendum faucibus purus sed commodo. Nullam at convallis ligula. Mauris metus est, adipiscing in pellentesque vitae, egestas sit amet est.</p>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed malesuada imperdiet justo non molestie. Pellentesque nisi arcu, euismod ac luctus a, sodales sit amet sem. Sed vestibulum urna in nisl auctor dignissim. Maecenas non libero ut urna faucibus feugiat. Nam vitae tortor mollis ante dictum scelerisque tempus eu sem. Vivamus ut ante eros, id porta massa. Sed ut lacus at elit tincidunt tempor. Curabitur facilisis, libero vel porttitor faucibus, sapien arcu convallis dolor, sed facilisis metus urna non tortor. Donec quis eros eu felis imperdiet elementum. Morbi bibendum imperdiet felis nec ultricies. Nullam placerat tempor nisl ac vehicula. In pellentesque felis sed purus vestibulum in commodo leo ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras pulvinar tellus quis augue luctus laoreet. Vivamus sed suscipit erat. Duis tempor dignissim commodo. Nullam bibendum faucibus purus sed commodo. Nullam at convallis ligula. Mauris metus est, adipiscing in pellentesque vitae, egestas sit amet est.</p>
    <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

@endsection
