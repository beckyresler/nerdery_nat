@extends('layouts.main')

@section('page-title', 'Page Not Found | ')

@section('content')
    <h1>Page Not Found</h1>
    <p>The page you are trying to view does not exist.</p>
@endsection
