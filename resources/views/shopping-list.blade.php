@extends('layouts.main')

@section('page-title', 'Shopping List | ')

@section('content')
    <h1>Shopping List for {{ \Carbon\Carbon::now(new \DateTimeZone(Config::get('app.timezone')))->format('F Y') }}</h1>
    {{-- were any API errors encountered? --}}
    @if(!empty($apiErrors))
        <div class="alert-box alert">
            <ul>
                <li>{!! implode('</li><li>', $apiErrors) !!}</li>
            </ul>
        </div>
    @else
        {{-- make sure there are snacks for the list --}}
        @if(!$snacksToPurchase->isEmpty())
            <table class="full-width">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Snack</th>
                    <th>Purchase Location</th>
                    <th>Notes</th>
                </tr>
            </thead>
            <tbody>
            @php $i = 1; @endphp
            @foreach($snacksToPurchase as $snack)
                <tr>
                    <td class="count">{{ $i }}</td>
                    <td>{{ $snack->name }}</td>
                    <td>{{ $snack->purchaseLocations }}</td>
                    <td>
                        @if(!$snack->optional) Always Purchased @else {{ $additionalSnacks->where('snack_id', $snack->id)->first()->voteCount }} Votes @endif
                    </td>
                </tr>
                @php $i++; @endphp
            @endforeach
            </tbody>
            </table>
        @else
            <p>There are no snacks on the shopping list.</p>
        @endif
    @endif
@endsection
