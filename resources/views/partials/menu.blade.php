@php $currentUrl = url()->current(); @endphp
<ul class="main-menu">
    <li @if($currentUrl == route('vote.show')) class="active"@endif ><a href="{{ route('vote.show') }}">Vote</a></li>
    <li @if($currentUrl == route('suggest.show')) class="active"@endif ><a href="{{ route('suggest.show') }}">Suggest</a></li>
    <li @if($currentUrl == route('shop.show')) class="active"@endif ><a href="{{ route('shop.show') }}">Shopping List</a></li>
</ul>