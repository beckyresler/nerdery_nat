<!doctype html>
<html lang="lang="{{ config('app.locale') }}"">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>@yield('page-title'){{ config('app.name') }} - Nerdery Snack Food Ordering System</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon.png">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.css" />
<link rel="stylesheet" type="text/css" href="/assets/css/app.css" />
<script src="/assets/js/modernizr.js"></script>
</head>
<body>
<header class="site-header" role="banner">
    <div class="row">
        <div class="large-7 columns">
            <div class="site-title">{{ config('app.name') }}</div>
            <div class="tagline">Nerdery Snack Food Ordering System</div>
        </div>
        <div class="large-5 columns">
            @include('partials.menu')
        </div>
    </div>
</header>
<div class="content-wrapper">
    <div class="row page-content" role="main">
        <div class="large-12 columns">
            @yield('content')
        </div>
    </div>
</div>
<footer class="site-footer" role="contentinfo">
    <div class="row">
        <div class="large-12 columns">
            &copy;{{ date('Y') }} The Nerdery
        </div>
    </div>
</footer>
</body>
<script src="/assets/js/fastclick.js"></script>
<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/foundation.js"></script>
<script src="/assets/js/app.js"></script>
</html>