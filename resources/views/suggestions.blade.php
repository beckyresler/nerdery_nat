@extends('layouts.main')

@section('page-title', 'Suggestions | ')

@section('content')
    <h1>Snack Suggestions for {{ \Carbon\Carbon::now(new \DateTimeZone(Config::get('app.timezone')))->format('F Y') }}</h1>

    {{-- are there any general session messages? --}}
    @if(session()->has('success'))
        <div class="alert-box success"><i class="fa fa-lg fa-check-circle"></i> {{ session()->get('success') }}</div>
    @endif

    @php
    // get the date the suggestion was made
    $suggestionMadeTimestamp    = $cookieInfo['suggestion-date'];
    $suggestionMade             = (!empty($suggestionMadeTimestamp)) ? \Carbon\Carbon::createFromTimeStamp($suggestionMadeTimestamp, Config::get('app.timezone')) : null;

    // verify that the suggestion was made in the current month (in case the cookie didn't expire properly)
    $now            = \Carbon\Carbon::now(new \DateTimeZone(Config::get('app.timezone')));
    $cookieExpired  = (!empty($suggestionMade)) ? (($suggestionMade->month!= $now->month || $suggestionMade->year != $now->year) ? true : false) : true;
    @endphp

    {{-- has the user already made a suggestion for this month? --}}
    @if($cookieInfo['suggestion-made'] && !$cookieExpired)
        <p>You have already suggested a snack for this month. You suggested &ldquo;{{ $cookieInfo['suggested-snack'] }}&rdquo; on {{ $suggestionMade->format('F j, Y') }} at {{ $suggestionMade->format('g:ia') }}.</p>
        <p><a href="{{ route('vote.show') }}" class="button small"><i class="fa fa-check-square-o"></i> Vote on this month's snacks</a></p>
    @else
        <p>You are allowed to suggest one snack per month.</p>

        {{-- were any API errors encountered? --}}
        @if(!empty($apiErrors))
            <div class="alert-box alert">
                <ul>
                    <li>{!! implode('</li><li>', $errors) !!}</li>
                </ul>
            </div>
        @else
            <div class="row">
                <div class="large-6 columns">
                    <h2>Select a Snack</h2>
                    {{-- are there any session messages related to this form? --}}
                    @if(session()->has('select_error'))
                        <div class="alert-box alert"><i class="fa fa-lg fa-exclamation-circle"></i> {!! session()->get('select_error') !!}</div>
                    @endif

                    {{-- make sure there are some suggestions remaining --}}
                    @if(!$snackSuggestions->isEmpty())

                        <form id="snack-suggestion" method="post" action="{{ route('suggest.select') }}">
                        {{ csrf_field() }}
                        <select name="snack_id" id="snack_id">
                            <option value="">-- Select a Snack --</option>
                            @foreach($snackSuggestions as $snack)
                                <option value="{{ $snack->id }}">{{ $snack->name }}</option>
                            @endforeach
                        </select>
                        <p><input type="submit" value="Suggest" class="button small" /></p>
                        </form>
                    @else
                        <div class="alert-box info"><i class="fa fa-info-circle"></i> All available snacks have already been suggested.</div>
                    @endif
                </div>
                <div class="large-6 columns">
                    <h2>Suggest a New Snack</h2>
                    {{-- are there any session messages related to this form? --}}
                    @if(session()->has('store_error'))
                        <div class="alert-box alert"><i class="fa fa-lg fa-exclamation-circle"></i> {!! session()->get('store_error') !!}</div>
                    @endif

                    {{-- are there any validation errors? --}}
                    {{-- this is the only form being validated using a FormRequest, so that can be sure the errors are related to it. --}}
                    @if($errors->any())
                        <div class="alert-box alert"><i class="fa fa-lg fa-exclamation-circle"></i> Your snack suggestion could not be added. Please address the following issues and try again.
                            <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                    <form id="new-snack-suggestion" method="post" action="{{ route('suggest.store') }}">
                    {{ csrf_field() }}
                    <p class="instructions">If you would like to suggest a new snack, fill out the form below. All fields are required.</p>
                    <p>
                        <label for="">Name</label>
                        <input type="text" name="snack_name" id="snack_name" value="{{ old('snack_name') }}" />
                    </p>
                    <p>
                        <label for="">Purchase Location</label>
                        <input type="text" name="purchase_location" id="purchase_location" value="{{ old('purchase_location') }}" />
                    </p>
                    <p><input type="submit" value="Suggest" class="button small" /></p>
                    </form>
                </div>
            </div>
        @endif
    @endif
@endsection
